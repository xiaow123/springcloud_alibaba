package com.xiaow.common.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xiaow.common.vo.AccountVo;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.io.UnsupportedEncodingException;
import java.util.*;

public class JwtUtils {

    public static final String TOKEN_HEADER = "token";
    public static final String TOKEN_PREFIX = "Bearer ";

    private static final String SUBJECT = "xiaow";

    private static final long EXPIRITION = 1000 * 24 * 60 * 60 * 7;   //过期时间

    private static final String APPSECRET_KEY = "xiaow_secret";  //盐值

    private static final String ROLE_CLAIMS = "roles";

    public static String generateJsonWebToken(AccountVo user) {
        String token = Jwts
                .builder()
                .setSubject(SUBJECT)
                .claim("urls", user.getUrls())
                .claim("id", user.getId())
                .claim("username", user.getUsername())
                .claim("password", user.getPassword())
                .claim("eamil", user.getEmail())
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRITION))
                .signWith(SignatureAlgorithm.HS256, APPSECRET_KEY).compact();
        return token;
    }

    public static Claims checkJWT(String token) {
        try {
            final Claims claims = Jwts.parser().setSigningKey(APPSECRET_KEY).parseClaimsJws(token).getBody();
            return claims;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取用户名
     *
     * @param token
     * @return
     */
    public static Claims getClaims(String token) {
        Claims claims = Jwts.parser().setSigningKey(APPSECRET_KEY).parseClaimsJws(token).getBody();
        return claims;
    }

    public static String getUsernameWithoutSecert(String token) {
        String payload= token.split("\\.")[1];
        String payloaddecode = null;
        try {
            payloaddecode = new String(Base64.getDecoder().decode(payload.getBytes()), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        JSONObject jsonObject1 =JSONObject.parseObject(payloaddecode);
        return jsonObject1.get("username").toString();
    }



    /**
     * 是否过期
     *
     * @param token
     * @return
     */
    public static boolean isExpiration(String token) {
        Claims claims = Jwts.parser().setSigningKey(APPSECRET_KEY).parseClaimsJws(token).getBody();
        return claims.getExpiration().before(new Date());
    }

    public static void main(String[] args) {
        List<String> urls=new LinkedList<>();
        urls.add("/blog");
        urls.add("/blog");
        urls.add("/blog");
        urls.add("/blog");
        urls.add("/blog");
        urls.add("/blog");
//        System.out.println(generateJsonWebToken(new AccountVo().setEmail("123").setUrls(urls)));
        Claims claims = getClaims("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ4aWFvdyIsInVybHMiOlsiL2Jsb2ciLCIvYmxvZyIsIi9ibG9nIiwiL2Jsb2ciLCIvYmxvZyIsIi9ibG9nIl0sImVhbWlsIjoiMTIzIiwiaWF0IjoxNjI2Nzg2MDM2LCJleHAiOjE2MjczOTA4MzZ9.vHa5CaVEtqNnw3I_Ft0VYwC0ZhzRJ1z80LA2psGPyZ4");
        System.out.println(claims.get("urls"));
    }


}

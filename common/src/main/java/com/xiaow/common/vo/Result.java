package com.xiaow.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class Result implements Serializable {
    private String info;
    private Integer code;
    private Object data;

    public static Result result(String info,Integer code,Object data) {
        Result r = new Result(info,code,data);
        return r;
    }

    public static void main(String[] args) {
        for (String s : "/one/**".split("/")) {
            System.out.println(s);
        }
    }

}

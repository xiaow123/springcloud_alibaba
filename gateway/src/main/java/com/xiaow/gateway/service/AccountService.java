package com.xiaow.gateway.service;

import com.xiaow.gateway.service.impl.AccountServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = "account",fallback = AccountServiceImpl.class)
public interface AccountService {
    @GetMapping("/urlandperm/getNoAuthUrls")
    public List<String> getNoAuthUrls();
}

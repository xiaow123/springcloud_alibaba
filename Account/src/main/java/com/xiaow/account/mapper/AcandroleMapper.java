package com.xiaow.account.mapper;

import com.xiaow.account.entity.Acandrole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 账户角色关联表 Mapper 接口
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
public interface AcandroleMapper extends BaseMapper<Acandrole> {

}

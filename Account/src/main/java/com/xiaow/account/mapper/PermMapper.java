package com.xiaow.account.mapper;

import com.xiaow.account.entity.Perm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 权限 Mapper 接口
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
public interface PermMapper extends BaseMapper<Perm> {

    List<Perm> findByAccountId(Integer accountid);

}

package com.xiaow.account;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.xiaow.account.mapper")
public class Account8090 {
    public static void main(String[] args) {
        SpringApplication.run(Account8090.class, args);
    }
}

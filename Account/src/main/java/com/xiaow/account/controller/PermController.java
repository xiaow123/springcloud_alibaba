package com.xiaow.account.controller;


import com.xiaow.account.entity.Perm;
import com.xiaow.account.service.PermService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 权限 前端控制器
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
@RestController
@RequestMapping("/perm")
public class PermController {

    @Autowired
    PermService permService;

    /**
     * 根据用户id获取用户的权限
     * @param accountid
     * @return
     */
    @GetMapping("/findByAccountId")
    List<Perm> findByAccountId(Integer accountid){
        return permService.findByAccountId(accountid);
    }

}

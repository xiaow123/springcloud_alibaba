package com.xiaow.account.service;

import com.xiaow.account.entity.Perm;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 权限 服务类
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
public interface PermService extends IService<Perm> {
    List<Perm> findByAccountId(Integer accountid);

}

package com.xiaow.account.service.impl;

import com.xiaow.account.entity.Roleandperm;
import com.xiaow.account.mapper.RoleandpermMapper;
import com.xiaow.account.service.RoleandpermService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
@Service
public class RoleandpermServiceImpl extends ServiceImpl<RoleandpermMapper, Roleandperm> implements RoleandpermService {

}

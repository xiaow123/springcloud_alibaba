package com.xiaow.account.service.impl;

import com.xiaow.account.entity.Role;
import com.xiaow.account.mapper.RoleMapper;
import com.xiaow.account.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}

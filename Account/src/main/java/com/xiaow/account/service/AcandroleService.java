package com.xiaow.account.service;

import com.xiaow.account.entity.Acandrole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 账户角色关联表 服务类
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
public interface AcandroleService extends IService<Acandrole> {

}

package com.xiaow.account.service.impl;

import com.xiaow.account.entity.Perm;
import com.xiaow.account.mapper.PermMapper;
import com.xiaow.account.service.PermService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 权限 服务实现类
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
@Service
public class PermServiceImpl extends ServiceImpl<PermMapper, Perm> implements PermService {

    @Override
    public List<Perm> findByAccountId(Integer accountid) {
        return baseMapper.findByAccountId(accountid);
    }
}

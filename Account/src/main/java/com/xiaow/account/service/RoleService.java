package com.xiaow.account.service;

import com.xiaow.account.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiaow
 * @since 2021-07-20
 */
public interface RoleService extends IService<Role> {

}
